class user {
  constructor() {
    this.configuration = new configuration();
    this.schrankStyleResponse = new Array();
  }

  getConfiguration() {
    console.log(this.configuration.Schubladenliste);
  }

  setProduct(product) {
    this.product = product;
  }

  getProduct() {
    return this.product;
  }

  setSchrankStyle(schrankStyle) {
    var j = 0;

    for (var i in schrankStyle) {
      if (schrankStyle[i] != null) {
        this.schrankStyleResponse[j] = schrankStyle[i];

        j++;
      }
    }
  }

  getSchrankStyle() {
    return this.schrankStyleResponse;
  }

  setSchrankBudget(minSchrankbudget, maxSchrankBudet) {
    this.minSchrankbudget = minSchrankbudget;

    this.maxSchrankBudet = maxSchrankBudet;
  }

  getSchrankBudget() {
    return {
      minSchrankBudget: this.minSchrankbudget,

      maxSchrankBudget: this.maxSchrankBudet,
    };
  }

  setSchrankRaumMasse(hoehe, breite, tiefe) {
    this.raumSchrankHoehe = hoehe;

    this.raumSchrankBreite = breite;

    this.raumSchrankTiefe = tiefe;
  }

  getSchrankRaumMasse() {
    return {
      schrankRaumHoehe: this.raumSchrankHoehe,

      schrankRaumBreite: this.raumSchrankBreite,

      schrankRaumTiefe: this.raumSchrankTiefe,
    };
  }

  setSchrankHolzart(holzart) {
    this.schrankHolzart = holzart;
  }

  getSchrankHolzart() {
    return this.schrankHolzart;
  }

  setSchrankGriffe(griffe) {
    this.schrankGriffe = griffe;
  }

  getSchrankGriffe() {
    return this.schrankGriffe;
  }
}
