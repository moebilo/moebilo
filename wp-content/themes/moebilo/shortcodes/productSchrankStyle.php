<?php
function productSchrankStyleFunc() {
?>
  <div class="mainSeitenView" id="productSchrankStyle">
  <h3>Wählen Sie maximal drei Styles auf?</h3>
    <div class="imgDiv" id="brounWoodenSchrank" style=" background-image: url('https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/brounWoodenSchrank.jpg');"> 
        <!-- <div class="selectKreisRahmen"><div class="selectKreisContent" id="selectKreisContent1" value="brounWoodenSchrank">✔️</div></div> -->
        <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/circle.png" alt="circle" style="width: 30px; height; 30px" id="schrankStyle1circle" class="schrankStyleCircle">
        <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/checkedCircle.png" alt="checked" style="width: 30px; height; 30px" id="schrankStyle1checked" class="schrankStyleChecked">
    </div>
    <div class="imgDiv" id="modernMirroredWhiteSchrank" style=" background-image: url('https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/modernMirroredWhiteSchrank.jpg');" >
        <!-- <div class="selectKreisRahmen"><div class="selectKreisContent" id="selectKreisContent2" value="modernMirroredWhiteSchrank">✔️</div></div> -->
        <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/circle.png" alt="circle" style="width: 30px; height; 30px" id="schrankStyle2circle" class="schrankStyleCircle">
        <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/checkedCircle.png" alt="checked" style="width: 30px; height; 30px" id="schrankStyle2checked" class="schrankStyleChecked">
    </div>
    <div class="imgDiv" id="openBrounModernSchrank" style=" background-image: url('https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/openBrounModernSchrank.jpg');" >
        <!-- <div class="selectKreisRahmen"><div class="selectKreisContent" id="selectKreisContent3" value="openBrounModernSchrank">✔️</div></div> -->
        <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/circle.png" alt="circle" style="width: 30px; height; 30px" id="schrankStyle3circle" class="schrankStyleCircle">
        <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/checkedCircle.png" alt="checked" style="width: 30px; height; 30px" id="schrankStyle3checked" class="schrankStyleChecked">
    </div>
    <div class="imgDiv"id="whiteModernSchrank"  style=" background-image: url('https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/whiteModernSchrank.jpg');" >
        <!-- <div class="selectKreisRahmen"><div class="selectKreisContent" id="selectKreisContent4" value="whiteModernSchrank">✔️</div></div> -->
        <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/circle.png" alt="circle" style="width: 30px; height; 30px" id="schrankStyle4circle" class="schrankStyleCircle">
        <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/checkedCircle.png" alt="checked" style="width: 30px; height; 30px" id="schrankStyle4checked" class="schrankStyleChecked">
    </div>
    <div class="imgDiv" id="woodenModernSchrank" style=" background-image: url('https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/brounWoodenSchrank.jpg');">
        <!-- <div class="selectKreisRahmen"><div class="selectKreisContent" id="selectKreisContent5" value="woodenModernSchrank">✔️</div></div> -->
        <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/circle.png" alt="circle" style="width: 30px; height; 30px" id="schrankStyle5circle" class="schrankStyleCircle">
        <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/checkedCircle.png" alt="checked" style="width: 30px; height; 30px" id="schrankStyle5checked" class="schrankStyleChecked">
    </div>
    <button id="productSchrankStyleWeiter"><i class="fas fa-arrow-right"></i></button>
  </div>
<?php 
} 
?>