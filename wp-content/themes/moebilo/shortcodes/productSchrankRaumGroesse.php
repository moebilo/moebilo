<?php
function productSchrankRaumGroesseFunc() {
?>
    <div class="mainSeitenView" id="productSchrankRaumGroesse">
      <div>
      <h2>Raum grösse</h2>
        <div>
          <label for="schrankRaumGroesseHoehe">Höhe [in cm]</label>
          <input type="number" value="200" min="100" max="400" step="5" id="schrankRaumGroesseHoeheNumber">
          <input type="range" value="200" min="100" max="400" step="5" id="schrankRaumGroesseHoeheRange"/>
        </div>
        <div>
          <label for="schrankRaumGroesseBreite">Breite [in cm]</label>
          <input type="number" value="300" min="100" max="2000" step="5" id="schrankRaumGroesseBreiteNumber">
          <input type="range" value="300" min="100" max="2000" step="5" id="schrankRaumGroesseBreiteRange"/>
        </div>
        <div>
          <label for="schrankRaumGroesseTiefe">Tiefe: [in cm]</label>
          <input type="number" value="300" min="100" max="2000" step="5" id="schrankRaumGroesseTiefeNumber">
          <input type="range" value="300" min="100" max="2000" step="5" id="schrankRaumGroesseTiefeRange"/>
        </div>
      </div>
      <button id="productSchrankRaumGroesseWeiter">Weiter</button>
    </div>
<?php 
} 
?>