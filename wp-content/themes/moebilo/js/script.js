jQuery(document).ready(function () {

/* 
----------------------------------
| ########### script ########### |
----------------------------------
*/

  let systemUser = new user();

  jQuery(".mainSeitenView").hide();
  jQuery("#chooseProduct").show();
  jQuery("#productSchrankNavigation").show()

  jQuery("#productBildSchrank").click(function () {
    jQuery("#chooseProduct").hide();
    jQuery("#productSchrankStyle").show();
  });

  jQuery("#productSchrankStyleWeiter").click(function () {
    jQuery("#productSchrankStyle").hide();
    jQuery("#productSchrankBudget").show();
  });

  jQuery("#productSchrankBudgetWeiter").click(function () {
    jQuery("#productSchrankBudget").hide();
    jQuery("#productSchrankRaumGroesse").show();
  });

  jQuery("#productSchrankRaumGroesseWeiter").click(function () {
    jQuery("#productSchrankRaumGroesse").hide();
    jQuery("#productSchrankchooseHolzart").show();
  });

  jQuery("#productSchrankchooseHolzartWeiter").click(function () {
    jQuery("#productSchrankchooseHolzart").hide();
    jQuery("#productSchrankchooseGriffe").show();
  });

  jQuery("#productSchrankchooseGriffeWeiter").click(function () {
    jQuery("#productSchrankchooseGriffe").hide();
    jQuery("#productSchrankKonfigurator").show();
  });

  /* 
----------------------------------
| ####### choose Product ####### |
----------------------------------
*/

  jQuery("#productBildSchrank").click(function () {
    systemUser.setProduct("Schrank");
    console.log(systemUser.getProduct());
  });

  /* 
-----------------------------------
| #### product Schrank Style #### |
-----------------------------------
*/

  var coundCheckedStyle = 0;
  var countMaxCheckedStyles = 3;

  jQuery(".schrankStyleChecked").hide();

  jQuery("#brounWoodenSchrank").click(function () {
    if (jQuery("#schrankStyle1circle").hasClass("checked")) {
      jQuery("#schrankStyle1circle").removeClass("checked");
      jQuery("#schrankStyle1checked").hide();
      jQuery("#schrankStyle1circle").show();
      coundCheckedStyle -= 1;
    } else {
      if(coundCheckedStyle < countMaxCheckedStyles){
      jQuery("#schrankStyle1circle").addClass("checked");
      jQuery("#schrankStyle1checked").show();
      jQuery("#schrankStyle1circle").hide();
      coundCheckedStyle += 1;
      }
    }
  });

  jQuery("#modernMirroredWhiteSchrank").click(function () {
    if (jQuery("#schrankStyle2circle").hasClass("checked")) {
      jQuery("#schrankStyle2circle").removeClass("checked");
      jQuery("#schrankStyle2checked").hide();
      jQuery("#schrankStyle2circle").show();
      coundCheckedStyle -= 1;
    } else {
      if(coundCheckedStyle < countMaxCheckedStyles){
        jQuery("#schrankStyle2circle").addClass("checked");
        jQuery("#schrankStyle2checked").show();
        jQuery("#schrankStyle2circle").hide();
        coundCheckedStyle += 1;
      }
      
    }
  });

  jQuery("#openBrounModernSchrank").click(function () {
    if (jQuery("#schrankStyle3circle").hasClass("checked")) {
      jQuery("#schrankStyle3circle").removeClass("checked");
      jQuery("#schrankStyle3checked").hide();
      jQuery("#schrankStyle3circle").show();
      coundCheckedStyle -= 1;
    } else {
      if(coundCheckedStyle < countMaxCheckedStyles){
        jQuery("#schrankStyle3circle").addClass("checked");
        jQuery("#schrankStyle3checked").show();
        jQuery("#schrankStyle3circle").hide();
        coundCheckedStyle += 1;
      }
    }
  });

  jQuery("#whiteModernSchrank").click(function () {
    if (jQuery("#schrankStyle4circle").hasClass("checked")) {
      jQuery("#schrankStyle4circle").removeClass("checked");
      jQuery("#schrankStyle4checked").hide();
      jQuery("#schrankStyle4circle").show();
      coundCheckedStyle -= 1;
    } else {
      if(coundCheckedStyle < countMaxCheckedStyles){
        jQuery("#schrankStyle4circle").addClass("checked");
        jQuery("#schrankStyle4checked").show();
        jQuery("#schrankStyle4circle").hide();
        coundCheckedStyle += 1;
      }
    }
  });

  jQuery("#woodenModernSchrank").click(function () {
    if (jQuery("#schrankStyle5circle").hasClass("checked")) {
      jQuery("#schrankStyle5circle").removeClass("checked");
      jQuery("#schrankStyle5checked").hide();
      jQuery("#schrankStyle5circle").show();
      coundCheckedStyle -= 1;
    } else {
      if(coundCheckedStyle < countMaxCheckedStyles){
        jQuery("#schrankStyle5circle").addClass("checked");
        jQuery("#schrankStyle5checked").show();
        jQuery("#schrankStyle5circle").hide();
      coundCheckedStyle += 1;
      }
    }
  });

  jQuery("#productSchrankStyleWeiter").click(function () {
    var value = [];
    var valueBool = [false, false, false, false, false]
    var valueDelivery = [];

    if(jQuery("#schrankStyle1circle").hasClass("checked")){
      value[0] = "brounWoodenSchrank";
      valueBool[0] = true;
    }
    if(jQuery("#schrankStyle2circle").hasClass("checked")){
      value[1] = "modernMirroredWhiteSchrank";
      valueBool[1] = true;
    }
    if(jQuery("#schrankStyle3circle").hasClass("checked")){
      value[2] = "openBrounModernSchrank";
      valueBool[2] = true;
    }
    if(jQuery("#schrankStyle4circle").hasClass("checked")){
      value[3] = "whiteModernSchrank";
      valueBool[3] = true;
    }
    if(jQuery("#schrankStyle5circle").hasClass("checked")){
      value[4] = "woodenModernSchrank";
      valueBool[4] = true;
    }

    systemUser.setSchrankStyle(value);
    console.log(systemUser.getSchrankStyle())
  });

  /* 
----------------------------------
| ### product Schrank Budget ### |
----------------------------------
*/

  jQuery("#schrankBudgetRangeMax").on("input", function () {
    jQuery("#schrankBudgetNumberMax").val(
      jQuery("#schrankBudgetRangeMax").val()
    );
  });

  jQuery("#schrankBudgetRangeMin").on("input", function () {
    jQuery("#schrankBudgetNumberMin").val(
      jQuery("#schrankBudgetRangeMin").val()
    );
  });

  jQuery("#productSchrankBudgetWeiter").click(function() {
    systemUser.setSchrankBudget(jQuery("#schrankBudgetRangeMin").val(), jQuery("#schrankBudgetRangeMax").val())
    console.log(systemUser.getSchrankBudget())
  })

/* 
-----------------------------------
| ##### Schrank Raum Grösse ##### |
-----------------------------------
*/

  jQuery("#schrankRaumGroesseHoeheRange").on("input", function () {
    jQuery("#schrankRaumGroesseHoeheNumber").val(
      jQuery("#schrankRaumGroesseHoeheRange").val()
    );
  });

  jQuery("#schrankRaumGroesseBreiteRange").on("input", function () {
    jQuery("#schrankRaumGroesseBreiteNumber").val(
      jQuery("#schrankRaumGroesseBreiteRange").val()
    );
  });

  jQuery("#schrankRaumGroesseTiefeRange").on("input", function () {
    jQuery("#schrankRaumGroesseTiefeNumber").val(
      jQuery("#schrankRaumGroesseTiefeRange").val()
    );
  });

  jQuery("#productSchrankRaumGroesseWeiter").click(function() {
    systemUser.setSchrankRaumMasse(jQuery("#schrankRaumGroesseHoeheRange").val(), jQuery("#schrankRaumGroesseBreiteRange").val(), jQuery("#schrankRaumGroesseTiefeRange").val())
    console.log(systemUser.getSchrankRaumMasse())
  })

  /* 
-----------------------------------
| ####### Schrank Holzart ####### |
-----------------------------------
*/

  jQuery("#productSchrankchooseHolzartWeiter").hide()

  jQuery("#holzart1").click(function(){
    systemUser.setSchrankHolzart("holzart1")
    jQuery("#productSchrankchooseHolzartWeiter").show()
  })

  jQuery("#holzart2").click(function(){
    systemUser.setSchrankHolzart("holzart2")
    jQuery("#productSchrankchooseHolzartWeiter").show()
  })

  jQuery("#holzart3").click(function(){
    systemUser.setSchrankHolzart("holzart3")
    jQuery("#productSchrankchooseHolzartWeiter").show()
  })

  jQuery("#holzart4").click(function(){
    systemUser.setSchrankHolzart("holzart4")
    jQuery("#productSchrankchooseHolzartWeiter").show()
  })

  jQuery("#holzart5").click(function(){
    systemUser.setSchrankHolzart("holzart5")
    jQuery("#productSchrankchooseHolzartWeiter").show()
  })

  jQuery("#holzart6").click(function(){
    systemUser.setSchrankHolzart("holzart6")
    jQuery("#productSchrankchooseHolzartWeiter").show()
  })

  jQuery("#productSchrankchooseHolzartWeiter").click(function() {
    console.log(systemUser.getSchrankHolzart())
  })


  /* 
----------------------------------
| ####### Schrank Griffe ####### |
----------------------------------
*/

  jQuery("#productSchrankchooseGriffeWeiter").hide()

  jQuery("#griff1").click(function(){
    systemUser.setSchrankGriffe("griff1")
    jQuery("#productSchrankchooseGriffeWeiter").show()
  })

  jQuery("#griff2").click(function(){
    systemUser.setSchrankGriffe("griff2")
    jQuery("#productSchrankchooseGriffeWeiter").show()
  })

  jQuery("#griff3").click(function(){
    systemUser.setSchrankGriffe("griff3")
    jQuery("#productSchrankchooseGriffeWeiter").show()
  })

  jQuery("#griff4").click(function(){
    systemUser.setSchrankGriffe("griff4")
    jQuery("#productSchrankchooseGriffeWeiter").show()
  })

  jQuery("#productSchrankchooseGriffeWeiter").click(function() {
    console.log(systemUser.getSchrankGriffe())
  })



  /* 
-----------------------------------
| ############# new ############# |
-----------------------------------
*/
});
