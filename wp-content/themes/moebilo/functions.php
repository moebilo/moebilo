<?php

include( get_stylesheet_directory() . '/shortcodes/userdata.php' );
include( get_stylesheet_directory() . '/shortcodes/choose_product.php' );
include( get_stylesheet_directory() . '/shortcodes/productSchrankStyle.php' );
include( get_stylesheet_directory() . '/shortcodes/productSchrankBudget.php' );
include( get_stylesheet_directory() . '/shortcodes/productSchrankRaumGroesse.php' );
include( get_stylesheet_directory() . '/shortcodes/productSchrankHolzart.php' );
include( get_stylesheet_directory() . '/shortcodes/productSchrankGriffe.php' );
include( get_stylesheet_directory() . '/shortcodes/productSchrankKonfigurator.php' );
include( get_stylesheet_directory() . '/shortcodes/productSchrankNavigation.php' );
renderShortcodes();


function renderShortcodes(){
  add_shortcode( 'userdataFunc', 'userdataFunc' );
  add_shortcode( 'chooseProductFunc', 'chooseProductFunc' );
  add_shortcode( 'productSchrankStyleFunc', 'productSchrankStyleFunc' );
  add_shortcode( 'productSchrankBudgetFunc', 'productSchrankBudgetFunc' );
  add_shortcode( 'productSchrankRaumGroesseFunc', 'productSchrankRaumGroesseFunc' );
  add_shortcode( 'productSchrankChooseHolzartFunc', 'productSchrankChooseHolzartFunc' );
  add_shortcode( 'productSchrankchooseGriffeFunc', 'productSchrankchooseGriffeFunc' );
  add_shortcode( 'productSchrankKonfiguratorFunc', 'productSchrankKonfiguratorFunc' );
  add_shortcode( 'productSchrankNavigationFunc', 'productSchrankNavigationFunc' );
}

function my_theme_enqueue_styles() {
  wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

function my_custom_scripts() {
    wp_enqueue_script( 'configuration-class-js', get_stylesheet_directory_uri() . '/js/classes/configuration.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'user-class-js', get_stylesheet_directory_uri() . '/js/classes/user.js', array( 'jquery' ),'',true );
    
    wp_enqueue_script( 'script-js', get_stylesheet_directory_uri() . '/js/script.js', array( 'jquery' ),'',true );
    // wp_enqueue_script( 'chooseProduct-js', get_stylesheet_directory_uri() . '/js/chooseProduct.js', array( 'jquery' ),'',true );
    // wp_enqueue_script( 'productSchrankStyle-js', get_stylesheet_directory_uri() . '/js/productSchrankStyle.js', array( 'jquery' ),'',true );
    // wp_enqueue_script( 'productSchrankBudget-js', get_stylesheet_directory_uri() . '/js/productSchrankBudget.js', array( 'jquery' ),'',true );
}
add_action( 'wp_enqueue_scripts', 'my_custom_scripts' );

// remove wp version number from scripts and styles
function remove_css_js_version( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_css_js_version', 9999 );
add_filter( 'script_loader_src', 'remove_css_js_version', 9999 );

// remove wp version number from head and rss
function artisansweb_remove_version() {
    return '';
}
add_filter('the_generator', 'artisansweb_remove_version');

// function my_custom_scripts() {
//   wp_register_script('my_amazing_script', get_template_directory_uri() . '/js/chooseProduct.js', array('jquery'),'1.1', true);
//   }
// add_action( 'wp_enqueue_scripts', 'my_custom_scripts' );
/**-----------------------------------------------------------*/

add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles', 100);

function salient_child_enqueue_styles() {
		
		$nectar_theme_version = nectar_get_theme_version();
		wp_enqueue_style( 'salient-child-style', get_stylesheet_directory_uri() . '/css/style.css', '', $nectar_theme_version );
		
    if ( is_rtl() ) {
   		wp_enqueue_style(  'salient-rtl',  get_template_directory_uri(). '/rtl.css', array(), '1', 'screen' );
		}
}

?>