Hier entsteht die Priceless-Version von [Möbilo](https://moebilo.ch/).

- Backend-Developer: [Mattia Müggler](https://mattiamueggler.ch/)
- Frontend-Developer: [Kay Wild](http://kaywild.netlify.app/)
- Frontend-Developer, Marketing und Desing: [Michael Schädler](https://michaelschaedler.li/)
- Verkauf, Engineering und Finanzen: Marco Bernold

© [Upcraft](https://upcraft.li/)
