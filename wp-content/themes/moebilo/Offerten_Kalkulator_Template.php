<?php /* Template Name: Offerten_Kalkulator_Template */ 

get_header(); ?>
<div id="primary" class="content-area">
  <!-- <script src="/js/chooseProduct.js"></script> -->

  <main id="main" class="site-main" role="main">

  <?php echo do_shortcode("[chooseProductFunc]"); ?>
  <?php echo do_shortcode("[productSchrankStyleFunc]"); ?>
  <?php echo do_shortcode("[productSchrankBudgetFunc]"); ?>
  <?php echo do_shortcode("[productSchrankRaumGroesseFunc]"); ?>
  <?php echo do_shortcode("[productSchrankChooseHolzartFunc]"); ?>
  <?php echo do_shortcode("[productSchrankchooseGriffeFunc]"); ?>
  <?php echo do_shortcode("[productSchrankKonfiguratorFunc]"); ?>
  <?php echo do_shortcode("[productSchrankNavigationFunc]"); ?>

  </main><!-- .site-main -->
<?php get_sidebar( 'content-bottom' ); ?>
</div><!-- .content-area -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
