<?php

/**testcomment by @Michaelschaedler */

/**

 * Grundeinstellungen für WordPress

 *

 * Zu diesen Einstellungen gehören:

 *

 * * MySQL-Zugangsdaten,

 * * Tabellenpräfix,

 * * Sicherheitsschlüssel

 * * und ABSPATH.

 *

 * Mehr Informationen zur wp-config.php gibt es auf der

 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}

 * Seite im Codex. Die Zugangsdaten für die MySQL-Datenbank

 * bekommst du von deinem Webhoster.

 *

 * Diese Datei wird zur Erstellung der wp-config.php verwendet.

 * Du musst aber dafür nicht das Installationsskript verwenden.

 * Stattdessen kannst du auch diese Datei als wp-config.php mit

 * deinen Zugangsdaten für die Datenbank abspeichern.

 *

 * @package WordPress

 */



// ** MySQL-Einstellungen ** //

/**   Diese Zugangsdaten bekommst du von deinem Webhoster. **/



/**

 * Ersetze datenbankname_hier_einfuegen

 * mit dem Namen der Datenbank, die du verwenden möchtest.

 */

define( 'DB_NAME', 'ezivozaw_wp25' );



/**

 * Ersetze benutzername_hier_einfuegen

 * mit deinem MySQL-Datenbank-Benutzernamen.

 */

define( 'DB_USER', 'root' );



/**

 * Ersetze passwort_hier_einfuegen mit deinem MySQL-Passwort.

 */

define( 'DB_PASSWORD', '' );



/**

 * Ersetze localhost mit der MySQL-Serveradresse.

 */

define( 'DB_HOST', 'localhost' );



/**

 * Der Datenbankzeichensatz, der beim Erstellen der

 * Datenbanktabellen verwendet werden soll

 */

define( 'DB_CHARSET', 'utf8mb4' );



/**

 * Der Collate-Type sollte nicht geändert werden.

 */

define('DB_COLLATE', '');



/**#@+

 * Sicherheitsschlüssel

 *

 * Ändere jeden untenstehenden Platzhaltertext in eine beliebige,

 * möglichst einmalig genutzte Zeichenkette.

 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * kannst du dir alle Schlüssel generieren lassen.

 * Du kannst die Schlüssel jederzeit wieder ändern, alle angemeldeten

 * Benutzer müssen sich danach erneut anmelden.

 *

 * @since 2.6.0

 */

define( 'AUTH_KEY',         '1~xYGKQjQI_`;QsS)4hVL$`}J-O/)yd+M;kZ$QEkM@,fXY1-SJx41E7goR&RrPJ=' );

define( 'SECURE_AUTH_KEY',  'r>6h{!;NxY>xg5Xx9n!aL}fh.ke|_*0/He@&;p<20sydz~~kNtEl}+|cfWZO`uj]' );

define( 'LOGGED_IN_KEY',    ':K!{MS.M0aJVwbq2A1{aUN`nNJpstp;Vn?H?IYG>[(SzSP$=Mz2RD0_ZX:cruP4x' );

define( 'NONCE_KEY',        '`f?4NJ>e;OVExc_pj.uF!Pn%w_Jcgcl<[@A]P1`c]y#P(W-Y&g-quw~Q!OdEqFT>' );

define( 'AUTH_SALT',        '3JyqJ.PE9cLgELS+!=RgLkOb,;+|JQ`*D7|n4@}IE:0/|/q?RsE#=B8Kgo%f/;AH' );

define( 'SECURE_AUTH_SALT', ' v7jIjpS[7A}KIh0/5RF Rq4MQ>p+t;y7S]@wsa>L:W+>o]> ?Y#vO`p<*tNR/OS' );

define( 'LOGGED_IN_SALT',   'Kk/[b,2H((rc2}`t/{^vp3UeNs;0wWp^x[dR{C|,T+g65]cRiR;!I :-s)#QaSY~' );

define( 'NONCE_SALT',       '<fDzZ@:BgcrsKi ,%#1t0Sncr2>pe=QFHCY7Fd@3^DUVK?(17l>wg97^,92{#XrS' );



/**#@-*/



/**

 * WordPress Datenbanktabellen-Präfix

 *

 * Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank

 * verschiedene WordPress-Installationen betreiben.

 * Bitte verwende nur Zahlen, Buchstaben und Unterstriche!

 */

$table_prefix = 'wp_';



/**

 * Für Entwickler: Der WordPress-Debug-Modus.

 *

 * Setze den Wert auf „true“, um bei der Entwicklung Warnungen und Fehler-Meldungen angezeigt zu bekommen.

 * Plugin- und Theme-Entwicklern wird nachdrücklich empfohlen, WP_DEBUG

 * in ihrer Entwicklungsumgebung zu verwenden.

 *

 * Besuche den Codex, um mehr Informationen über andere Konstanten zu finden,

 * die zum Debuggen genutzt werden können.

 *

 * @link https://codex.wordpress.org/Debugging_in_WordPress

 */

define( 'WP_DEBUG', true );

define( 'WP_DEBUG_DISPLAY', true );



/* Das war’s, Schluss mit dem Bearbeiten! Viel Spaß. */

/* That's all, stop editing! Happy publishing. */



/** Der absolute Pfad zum WordPress-Verzeichnis. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

}



/** Definiert WordPress-Variablen und fügt Dateien ein.  */

require_once( ABSPATH . 'wp-settings.php' );

