<?php
function chooseProductFunc() {
  ?>
    <div id='chooseProduct' class='chooseProduct mainSeitenView'>
    <div id="chooseProductImg">
      <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/schrank.png" alt="schrank" class="productBild" id="productBildSchrank">
      <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/stuhl.png" alt="stuhl" class="productBild" id="productBildStuhl">
      <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/bett.png" alt="bett" class="productBild" id="productBildBett">
      <img src="https://priceless.moebelkonfigurator.li/wp-content/uploads/2021/05/tisch.png" alt="tisch" class="productBild" id="productBildTisch">
    </div>
  </div>
<?php
}
?>