<?php
function productSchrankBudgetFunc() {
?>
  <div class="mainSeitenView" id="productSchrankBudget">
      <div>
        <div>
          <label for="schrankBudgetMin">mindestes Schrank Budet</label>
          <input type="number" value="2000" min="1000" max="10000" step="50" id="schrankBudgetNumberMin">
          <input type="range" value="2000" min="1000" max="10000" step="50" id="schrankBudgetRangeMin"/>
        </div>
        <div>
          <label for="schrankBudgetMax">maximales Schrank Budet</label>
          <input type="number" value="2000" min="1000" max="10000" step="50" id="schrankBudgetNumberMax">
          <input type="range" value="2000" min="1000" max="10000" step="50" id="schrankBudgetRangeMax"/>
        </div>
      </div>
      <button id="productSchrankBudgetWeiter">Weiter</button>
  </div>
<?php 
} 
?>